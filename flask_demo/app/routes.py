from app import app
from flask import render_template, send_from_directory, jsonify, json, request
import os
import time

questions = {
    '1': {'id': 1, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '2': {'id': 2, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '3': {'id': 3, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '4': {'id': 4, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '5': {'id': 5, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '6': {'id': 6, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '7': {'id': 7, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
     '8': {'id': 8, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '9': {'id': 9, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '10': {'id': 10, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '11': {'id': 11, 'type': '峽谷', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '12': {'id': 12, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '13': {'id': 13, 'type': '峽谷', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '14': {'id': 14, 'type': '峽谷', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '15': {'id': 15, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '16': {'id': 16, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '17': {'id': 17, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '18': {'id': 18, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '19': {'id': 19, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '20': {'id': 20, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '21': {'id': 21, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '22': {'id': 22, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '23': {'id': 23, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '24': {'id': 24, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '25': {'id': 25, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '26': {'id': 26, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '27': {'id': 27, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '28': {'id': 28, 'type': '山地', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '29': {'id': 29, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '30': {'id': 30, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '31': {'id': 31, 'type': '雪', 'description': '雪崩' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '32': {'id': 32, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '33': {'id': 33, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '34': {'id': 34, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '35': {'id': 35, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '36': {'id': 36, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '37': {'id': 37, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '38': {'id': 38, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '39': {'id': 39, 'type': '火山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '40': {'id': 40, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '41': {'id': 41, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '42': {'id': 42, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '43': {'id': 43, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '44': {'id': 44, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '45': {'id': 45, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '46': {'id': 46, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '47': {'id': 47, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '48': {'id': 48, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '49': {'id': 49, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '50': {'id': 50, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '51': {'id': 51, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '52': {'id': 52, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '53': {'id': 53, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '54': {'id': 54, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '55': {'id': 55, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '56': {'id': 56, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '57': {'id': 57, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '58': {'id': 58, 'type': '冰山', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '59': {'id': 59, 'type': '天氣', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '60': {'id': 60, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '61': {'id': 61, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '62': {'id': 62, 'type': '雪', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
    '63': {'id': 63, 'type': '玄武', 'description': '事件一' ,'ans':['蹲下','站好','呼叫無線電','趴下']},
}
answer = {
    '1': [0, 0, 0, -1],
    '2': [0, 0, 0, -1],
    '3': [0, 0, 0, -1],
    '4': [0, 0, 0, -1],
    '5': [0, 0, 0, -1],
    '6': [0, 0, 0, -1],
    '7': [0, 0, 0, -1],
    '8': [0, 0, 0, -1],
    '9': [0, 0, 0, -1],
    '10': [0, 0, 0, -1],
    '11': [0, 0, 0, -1],
    '12': [0, 0, 0, -1],
    '13': [0, 0, 0, -1],
    '14': [0, 0, 0, -1],
    '15': [0, 0, 0, -1],
    '16': [0, 0, 0, -1],
    '17': [0, 0, 0, -1],
    '18': [0, 0, 0, -1],
    '19': [0, 0, 0, -1],
    '20': [0, 0, 0, -1],
    '21': [0, 0, 0, -1],
    '22': [0, 0, 0, -1],
    '23': [0, 0, 0, -1],
    '24': [0, 0, 0, -1],
    '25': [0, 0, 0, -1],
    '26': [0, 0, 0, -1],
    '27': [0, 0, 0, -1],
    '28': [0, 0, 0, -1],
    '29': [0, 0, 0, -1],
    '30': [0, 0, 0, -1],
    '31': [0, 0, 0, -1],
    '32': [0, 0, 0, -1],
    '33': [0, 0, 0, -1],
    '34': [0, 0, 0, -1],
    '35': [0, 0, 0, -1],
    '36': [0, 0, 0, -1],
    '37': [0, 0, 0, -1],
    '38': [0, 0, 0, -1],
    '39': [0, 0, 0, -1],
    '40': [0, 0, 0, -1],
    '41': [0, 0, 0, -1],
    '42': [0, 0, 0, -1],
    '43': [0, 0, 0, -1],
    '44': [0, 0, 0, -1],
    '45': [0, 0, 0, -1],
    '46': [0, 0, 0, -1],
    '47': [0, 0, 0, -1],
    '48': [0, 0, 0, -1],
    '49': [0, 0, 0, -1],
    '50': [0, 0, 0, -1],
    '51': [0, 0, 0, -1],
    '52': [0, 0, 0, -1],
    '53': [0, 0, 0, -1],
    '54': [0, 0, 0, -1],
    '55': [0, 0, 0, -1],
    '56': [0, 0, 0, -1],
    '57': [0, 0, 0, -1],
    '58': [0, 0, 0, -1],
    '59': [0, 0, 0, -1],
    '60': [0, 0, 0, -1],
    '61': [0, 0, 0, -1],
    '62': [0, 0, 0, -1],
    '63': [0, 0, 0, -1],
}

people = [{'token': '', 'rounds': 0, 'route': 0, 'log': [], 'HP': 0, 'position': '','SAIN':0},
          {'token': '', 'rounds': 0, 'route': 0,
              'log': [], 'HP': 0, 'position': '','SAIN':0},
          {'token': '', 'rounds': 0, 'route': 0,
              'log': [], 'HP': 0, 'position': '','SAIN':0},
          {'token': '', 'rounds': 0, 'route': 0, 'log': [], 'HP': 0, 'position': '','SAIN':0}]
game = {'players': people, 'rounds': 1}
routes = [{'start': 1, 'end': 8}, {'start': 10, 'end': 18}, {
    'start': 2, 'end': 10}, {'start': 3, 'end': 4}, {'start': 5, 'end': 6}]


def init():
    initOK = False
    token = str(time.time())
    for i in range(4):
        if game['players'][i]['token'] == '':
            game['players'][i]['token'] = token
            game['players'][i]["HP"] = 5
            game['players'][i]["SAIN"] = 100
            initOK = True
            break

    if initOK == False :
        return "-1"
    return token


@app.route('/api/getToken', methods=['POST'])
def api_getToken():
    token = init()
    if token == '-1':
        return jsonify({"status":"fail"})
    return jsonify(token)


@app.route('/api/setRoute', methods=['POST'])
def api_setRoute():
    for player in game['players']:
        print(request.form)
        if(player['token'] == request.form['token']):
            player['route'] = request.form['route']
            player['position'] = routes[int(request.form['route'])]['start']
            return jsonify({'status': 'ok'})

    return jsonify({'status': 'fail'})


@app.route('/api/getRounds', methods=['POST'])
def api_getRounds():
    token = str(request.form['token'])
    for player in game['players']:
        if player['token'] == token:
            if player['HP'] < 0:
                return '-1'  # lose
    for player in game['players']:
        if player['token'] == token:
            if player['position'] == routes[int(player['route'])]['end']:
                return '-2'  # win
        else:
            if player['position'] == routes[int(player['route'])]['end']:
                return '-1'  # lose
    for player in game['players']:
        if player['token'] == token:
            if(game['rounds'] > player['rounds']):
                return '1'
            else:
                return '0'

    return '0'


@app.route('/api/getGame', methods=['POST'])
def getGame():
    return jsonify(game)


@app.route('/api/question/<path:path>', methods=['GET', 'POST'])
def api_question(path):
    return jsonify(questions[path])


@app.route('/api/reply', methods=['POST'])
def reply():
    qid = str(request.form['QuesionID'])
    aid = int(request.form['AnswerID'])
    token = str(request.form['token'])
    for player in game['players']:
        if(player['token'] == token):
            player['SAIN'] -= 10
            player['HP'] += answer[qid][aid]
            player['log'] += [{'event':qid,'description':questions[qid]['description']}]
            player['position'] = qid
            player['rounds'] += 1
            isRoundsDone = True
            for player in game['players']:
                if player['HP'] == 0:
                    continue
                if player['rounds'] < game['rounds']:
                    isRoundsDone = False

            if isRoundsDone == True:
                game['rounds'] += 1

            return jsonify({'status': 'ok'})
    return jsonify({'status': 'fail'})

@app.route('/api/getLog', methods=['POST'])
def api_getLog():
    token = str(request.form['token'])
    for player in game['players']:
        if player['token']==token:
            return jsonify(player['log'])
    return jsonify({})

@app.route('/api/getHP', methods=['POST'])
def api_getHP():
    token = str(request.form['token'])
    for player in game['players']:
        if player['token']==token:
            return jsonify(player['HP'])
    return jsonify(0) 

@app.route('/api/getSAIN', methods=['POST'])
def api_getSAIN():
    token = str(request.form['token'])
    for player in game['players']:
        if player['token']==token:
            return jsonify(player['SAIN'])
    return jsonify(0) 

@app.route('/api/member.json')
def api1():
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url = os.path.join(SITE_ROOT, "templates/data", "member.json")
    data = json.load(open(json_url))
    return jsonify(data)

@app.route('/api/killAll', methods=['GET' ,'POST'])
def killAll():
    game['rounds'] = 1
    for player in game['players']:
        player['token'] = ''
        player['rounds'] = 0
        player['route'] = 0
        player['log'] = []
        player['HP'] = 0
        player['position'] = ''
    
    return jsonify({'status':'ok'})


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<path:path>')
def sendPage(path):
    return render_template((path))


@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('templates/img', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('templates/css', path)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('templates/js', path)
